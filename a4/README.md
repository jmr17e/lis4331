# LIS4331

## Julia Riccio

### Assignment 4 Requirements:

*Four Parts:*

1. Create an app using Android Studio with a Splash screen
2. App will calculate interest payments on a home mortgage with data validation
3. Skill sets 10-12

#### Assignment Screenshots:

*My App:*

![video](img/video.gif)

*Skillsets:*

|Skillset 10|Skillset 11|Skillset 12|
|----------|----------|----------|
|![Skillset 7](img/ss10.png)       |![Skillset 5](img/ss11.png)          |![Skillset 12](img/ss12.png)

