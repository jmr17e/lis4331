# LIS4381

## Julia Riccio

### Project 2 Requirements:

*Four Parts:*

1. Create an app that allows user to view, update, add, and delete users.
2. Customize theme of app.

#### Assignment Screenshots:

*App Running*

![gif](img/p2.gif)

