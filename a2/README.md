# LIS4331

## Julia Riccio

### Assignment 2 Requirements:

*Two Parts:*

1. Create an app with Android Studio that calculates the amount each guest must pay with the tip added onto bill total.
2. SS1-3 (Java programs)
3. Chapter Questions (Chs. 3 & 4)

#### Assignment Screenshots:

*My Recipe App:*

|   First User Interface    |   Second User Interface   |
|---|---|
|![First UI](img/ui1.png)|![Second UI](img/ui2.png)   |


*Skillsets:*

|Skillset 1|Skillset 2|Skillset 3|
|----------|----------|----------|
|![Skillset 1](img/ss1.png)       |![Skillset 2](img/ss2.png)          |![Skillset 3](img/ss3.png)




