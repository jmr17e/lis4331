<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Julia Riccio">
		<link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment 1</title>
		<?php include_once("../css/include_css.php"); ?>
	</head>

	<body>
	<body style="background-image: url(img/bckgrnd1.jpg);">
	<?php include_once("../global/nav.php"); ?>
	

	<div class="contatiner">
		<div class="starter-template">
			<div class="page-header">
			<?php include_once("global/header.php"); ?>
			</div>
			<p class="text-justify">
			<strong>Three Parts:</strong>
<br>
1. Distributed version control with Git and BitBucket
<br>
2. Development Installations
<br>
3. Chapter Questions (Chs. 1 & 2)
<br>
* Screenshot of AMPPS Installation
<br>
* Screenshot of running java Hello;
<br>
* Screenshot of running Android Studio - My First App
<br>
* Bitbucket repo links: a) this assignment and b) the completed tutorials above (bitbucketstationlocations)
<br>
Git commands w/short descriptions:
<br>

1. git init: creates a new Git repository and can initialize a new repository
<br>
2. git status: displays state of working directory and staging area
<br>
3. git add: marks changes to be included in next commit
<br>
4. git commit: used to save your changes to local repository
<br>
5. git push: used to upload local repository content to remote repository
<br>
6. git pull: used to transfer these commits from local to remote repository
<br>
7. git branch: lists all local branches in current repository

			</p>

			<h4>Java Installation</h4>
			<img src="img/jdk_install.png" class="img-responsive center-block" alt="JDK installation">

			<h4>Android Studio Installation</h4>
			<img src="img/android.png" class="img-responsive center-block" alt="Android Studio Installation">

			<h4>AMPPS Installation</h4>
			<img src="img/ampps.png" class="img-responsive center-block" alt="AMPPS installation">

			<?php include_once "global/footer.php"; ?>
			</div>
			</div>

			<?php include_once("../js/include_js.php"); ?>
			</body>
			</html>
