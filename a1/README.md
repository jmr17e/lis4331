
# LIS4331

## Julia Riccio

### Assignment 1 Requirements:

*Four parts:*

1. Distributed version control with Git and BitBucket
2. Development Installations
3. Chapter Questions (Chs. 1 & 2)
4. Bitbucket repo links: a) this assignment and the completed tutorials above (bitbucketstationlocations)

|README.md file should include the following items:|Git Commands w/ short descriptions:
|--------------------------------------------------| ----------------------------------|
|* Screenshot of AMPPS Installation | 1. git init: creates a new Git repository and can initialize a new repository
|* Screenshot of running java Hello; | 2. git status: displays state of working directory and staging area
|* Screenshot of running Android Studio - My First App | 3. git add: marks changes to be included in next commit
|* git commands w/ short descriptions | 5. git push: used to upload local repository content to remote repository
|                                       | 6. git pull: used to transfer these commits from local to remote repository
|                                       | 7. git branch: lists all local branches in current repository


#### Assignment Screenshots:

|*Screenshot of running java Hello*:|*Screenshot of Android Studio - My First App*:
|-----------------------------------|-----------------------------------|
|![JDK Installation Screenshot](img/jdk_install.png)|![Android Studio Installation Screenshot](img/android.png)


|*Screenshot of Android Studio - My Contacts App; First UI*:|*My Contacts App; Second UI*:
|-----------------------------------|-----------------------------------|
|![Android Studio Installation Screenshot](img/contacts1.png)|![Android Studio Installation Screenshot](img/contacts2.png)|


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/jmr17e/bitbucketstationlocations/ "Bitbucket Station Locations")


