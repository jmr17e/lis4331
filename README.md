
# LIS4331 - Advanced Mobile Web App Development

## Julia Riccio

### LIS4331 Requirements:

*Course Work Links:*

1. [A1 README.md](./a1/README.md)

    * Install AMPPS

    * Install JDK

    * Install Android Studio and create My First App and Contacts App

    * Provide screenshots of installations

    * Create Bitbucket repo

    * Complete Bitbucket tutorials (bitbucketstationlocations and myteamquotes)

    * Provide git command descriptions

2. [A2 README.md](./a2/README.md)

    * Create Android app that calculates the amount each person must pay when splitting bill

    * Provide screenshots of completed app

    * Provide screenshots of SS1-3

3. [A3 README.md](./a3/README.md)

    * Create Android app that converts input of US Dollars to Euros, Pesos, and Canadian with Toast notification if input is > 100,000.

    * Provide screenshots of completed app, with Splash screen

    * Provide screenshots of SS4-6


4. [P1 README.md](./p1/README.md)

    * Create Android app that displays a Splash screen, a main activity screen with three artists, giving the user option to play/pause music 

    * Provide screenshots of completed app, with Splash screen

    * Provide screenshots of SS7-9

    
5. [A4 README.md](./a4/README.md)

    * Create Android app that displays a Splash screen, a main activity that prompts user to enter their mortgage payment, interest rate, and principle

    * Provide screenshots of completed app that includes validation (10, 20, or 30 only for years)

    * Provide screenshots of SS10-12


6. [A5 README.md](./a5/README.md)

    * Create an app that displays a RSS feed

    * Customize app theme

    * Provide screenshots of SS13-15

7. [P2 README.md](./p2/README.md)

    * Create an app that allows you to add, edit, update, and delete users

    * Customize app theme




