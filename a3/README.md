# LIS4331

## Julia Riccio

### Assignment 3 Requirements:

*Four Parts:*

1. Create android app that converts US Dollars to Euros, Pesos, and Canadian. App must also display Toast notification for inputs > 100,000 and have a Splash screen displaying for up to 3 seconds.
3. Provide screenshots of skillsets 4-6 (Java programs).
4. Chapter Questions (Chs. 5 & 6)

#### Assignment Screenshots:

*My App:*

|   Splash Screen:    | Toast Notification:      |  Conversion:     |
|------------------------------------------|------------------------------------------|-----------------|
|![splashScreen](img/splash.png)                      |![toastNotification](img/toast.png)                      |![conversion](img/convert.png)  |



*Skillsets:*

|Skillset 4|
|----------|
|![ss4](img/ss4.png)|

|   Skillset 5 |    Prompt Begins   |  Result       |           
|--------------|--------------------|-----------|
|![ss51](img/ss51.png)|![ss52](img/ss52.png)|![ss53](img/ss53.png)|

|   Skillset 6 |    Prompt Begins   |   Length Input |
|--------------|--------------------|----------------|
|![ss61](img/ss61.png)|![ss62](img/ss62.png)|![ss63](img/ss63.png)|

|   Width Input |   Height Input    |   Result  |
|---------------|-------------------|-----------|
|![ss64](img/ss64.png)|![ss65](img/ss65.png)|![ss66](img/ss66.png)|



