<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Julia Riccio">
		<link rel="icon" href="favicon.ico">

		<title>LIS4381 - Project 1</title>
		<?php include_once("../css/include_css.php"); ?>
	</head>

	<body>
	<?php include_once("../global/nav.php"); ?>
	<body style="background-image: url(img/bckgrnd1.jpg);">

	<div class="contatiner">
		<div class="starter-template">
			<div class="page-header">
			<?php include_once("global/header.php"); ?>
			</div>
			<p class="text-justify">
			<strong>Three Parts:</strong>
<br>
1. Create a mobile app which shows users your Business Card, providing the details of your name, interests, contact info, and school credentials.
<br>
2. Provide screenshots of skillsets 7-9 (Java programs).
<br>
3. Chapter Questions (Chs. 7 & 8)
<br>
* Course title, your name, assignment requirements, as per A1;
<br>
* Screenshot of running application's first interface;
<br>
* Screenshot of running application's second interface;
<br>
* Screenshots of Skillsets 7-9
			</p>


			<h4>My Business Card</h4>
			<table>
			<tr><td><img src="img/i1.png" class="img-responsive center-block" alt="First user interface"></td><td><img src="img/i2.png" class="img-responsive center-block" alt="Second user interface"></td></tr>
			</table>

			<h4>Skillsets</h4>
			<table>
			<tr><td><img src="img/ss7.png" class="img-responsive center-block" alt="SS7"></td><td><img src="img/ss8.png" class="img-responsive center-block" alt="SS8"></td><td><img src="img/ss9.png" class="img-responsive center-block" alt="SS9"></td></tr>
			</table>

			<?php include_once "global/footer.php"; ?>
			</div>
			</div>

			<?php include_once("../js/include_js.php"); ?>
			</body>
			</html>
