
# LIS4331

## Julia Riccio

### Project 1 Requirements:

*Three Parts:*

1. Create a mobile app which shows users a Splash screen, three artists, and option to play/pause music from artists.
2. Provide screenshots of skillsets 7-9 (Java programs).
3. Chapter Questions (Chs. 7 & 8)

#### Assignment Screenshots:

*My Music App:*

|   Splash Screen:    | Opening Screen:      |
|------------------------------------------|------------------------------------------|
|![UI1](img/splash.png)                      |![UI2](img/main.png)                      |     

| Playing Screen | Pause Screen |
|----------------|--------------|
|![ui3](img/play.png)|![ui4](img/pause.png)|



*Skillsets:*

|Skillset 7|Skillset 8|
|----------|----------|
|![Skillset 7](img/ss7.png)       |![Skillset 5](img/ss8.png)          |

|Skillset 9: Unselected List|Single Interval Selection|Multiple Interval Selection|
|---------------------------|-------------------------|-------------------|
|![ui1](img/ss91.png)|![ui1](img/ss92.png)|![ui3](img/ss93.png)|



